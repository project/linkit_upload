***********
* README: *
***********

DESCRIPTION:
------------
This module adds the possibility to upload files from the Linkit modal
when the managed files search plugin is enabled in the Linkit Profile.


INSTALLATION:
-------------
1. Place the entire linkit_upload directory into your Drupal sites/all/modules/
   directory.

2. Check if Linkit module is enabled. If not, enable by navigating to:
    administer > modules

3. Enable the linkit_upload module by navigating to:
    administer > modules

4. Edit your linkit profile and enable 'Managed files' search plugin.

Author:
-------
Stijn Stroobants (https://www.drupal.org/u/stijnstroobants)
stijn@stijnstroobants.com
