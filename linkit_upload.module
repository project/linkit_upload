<?php

/**
 * @file
 * Main file for linkit_upload module.
 */

/**
 * Implements hook_help().
 */
function linkit_upload_help($path, $arg) {
  switch ($path) {
    case 'admin/help#linkit_upload':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>';
      $output .= t('Linkit Upload is an extention to Linkit. It adds the possibility to upload files from the Linkit modal when the \'Managed files\' search-plugin is enabled.');
      $output .= '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_alter().
 */
function linkit_upload_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == "linkit_dashboard_form") {
    $profile = linkit_get_active_profile();
    if (isset($profile->data['search_plugins']['entity:file']['enabled']) && $profile->data['search_plugins']['entity:file']['enabled'] == 1) {
      if (isset($form['#suffix'])) {
        $form['#suffix'] .= drupal_render(drupal_get_form('linkit_upload_file_upload'));
      }
      else {
        $form['#suffix'] = drupal_render(drupal_get_form('linkit_upload_file_upload'));
      }
    }
  }
}

/**
 * Generate upload form.
 */
function linkit_upload_file_upload($form, &$form_state) {
  $form['im-container'] = array(
    '#prefix' => '<div id="im-area">',
    '#suffix' => '</div>',
  );

  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('File Upload'),
    '#description' => t('Upload a file'),
  );

  $form['upload'] = array(
    '#type' => 'submit',
    '#value' => 'upload',
    '#submit' => array('linkit_upload_upload_image'),
    '#ajax' => array(
      'callback' => 'linkit_upload_upload_image',
      'wrapper' => 'im-area',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  return $form;
}

/**
 * Upload form submit callback.
 */
function linkit_upload_upload_image($form, $form_state) {
  $extensions = variable_get('file_entity_default_allowed_extensions', '');
  $file = file_save_upload('file', array('file_validate_extensions' => array($extensions)), 'public://', FILE_EXISTS_REPLACE);

  if ($file) {
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    $form['im-container'] = array(
      '#title' => t('Preview:'),
      '#prefix' => '<div id="im-area">',
      '#markup' => t('File uploaded'),
      '#suffix' => '</div>',
    );
  }
  else {
    drupal_set_message(t('No file uploaded.'));
  }

  return $form['im-container'];
}
